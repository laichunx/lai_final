﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftingButtonClick : MonoBehaviour
{
    public Button confirmButton;
    public craftingInter ci;
    public Items item;

	void Start () {
        ci = GetComponent<craftingInter>();
        Debug.Log(ci);
        Debug.Log(item);
		confirmButton.onClick.AddListener(TaskOnClick);
	}

	void TaskOnClick(){
		Debug.Log(item.type);
	}
}
