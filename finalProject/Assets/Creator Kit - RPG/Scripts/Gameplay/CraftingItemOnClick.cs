﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftingItemOnClick : MonoBehaviour
{
    public craftingInter ci;
    public Button yourButton;
    public Items item;

	void Start () {
        ci = GetComponent<craftingInter>();
        item = yourButton.GetComponent<Items>();
		Button btn = yourButton.GetComponent<Button>();
		btn.onClick.AddListener(TaskOnClick);
	}

	void TaskOnClick(){
        ci.updateData(item);
	}
}
