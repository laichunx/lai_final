﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crafting : MonoBehaviour
{
    public bool craftingEnabled;
    public GameObject crafting;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.C))
        {
            craftingEnabled = !craftingEnabled;
        }

        if(craftingEnabled == true)
        {
            crafting.SetActive(true);
        } else {
            crafting.SetActive(false);
        }
    }
}
